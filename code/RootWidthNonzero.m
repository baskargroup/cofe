function [ Awidth,Rownonzero ] = RootWidthNonzero( Rootimage,Length )
%% Measures width
% Width measurement
% preallocate variables
Awidth=nan(Length,1);Rownonzero=nan(Length,1);
% identify the leftmost and rightmost non-zero pixels as function of depth
for i=1:Length
[~,col]=find(Rootimage(i,:)>0); 
if size(col,1)>0
Awidth(i)=max(col)-min(col);
Rownonzero(i)=length(col);
end
end
end

